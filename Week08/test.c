
#include "parser.h"

int main(int argc, const char** argv)
{
    lexer L;
    int i;
    for (i=1; i<argc; i++) {
        init_lexer(&L, argv[i]);
        if (!L.infile) {
            fprintf(stderr, "Couldn't open file %s\n", argv[i]);
            continue;
        }
        for (;;) {
            pstmt s = statement(&L);
            if (!s) break;
            if (s->rhs)     printf("Assignment\n");
            else            printf("Print\n");
        }
    }
}

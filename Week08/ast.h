#ifndef AST_H
#define AST_H

#include "lexer.h"

/*
 * To add type checking: need rules for types
 *  what are the types (in this case, int and real)
 *  what are allowed on the types i.e.
 *          int +,-,* int           : int
 *          int / int               : real
 *          real +, -, *, / real    : real
 *          int + real   ? not allowed
 *
 *  var: what are their types?
 *
 *      a..o : int
 *      p..z : real
 *
 * The main thing:
 *      int vars: can be assigned int values
 *     real vars: can be assigned real values only
 *
 */

typedef struct expr* pexpr;
struct expr {
    bool  is_integer;       // true: this has integer type otherwise real
    token optor;
    pexpr left;         // null for unary minus, leaves
    pexpr right;        // null for leaves
};


/*
 * Build an expr node for an integer literal.
 */
pexpr int_expr(token value);

/*
 * Build an expr node for a variable.
 */
pexpr var_expr(token var);

/*
 * Build an expr node for a unary operator
 */
pexpr unary_expr(token optor, pexpr opnd);

/*
 * Build an expr node for a binary operator
 */
pexpr binary_expr(pexpr left, token optor, pexpr right);



typedef struct stmt* pstmt;
struct stmt {
    unsigned var;
    pexpr rhs;      // null if printing
    unsigned lineno;
};

/*
 * Build a stmt node for an assignment.
 */
pstmt assign_stmt(unsigned var, pexpr rhs, token semi);

/*
 * Build a stmt node for print a variable.
 */
pstmt print_stmt(unsigned var, token semi);

#endif

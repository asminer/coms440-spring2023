
#include "parser.h"

#include <stdlib.h>

void error(lexer* L, const char* expect)
{
    fprintf(stderr, "Syntax error file %s line %u:\n\tExpected %s, saw ",
                L->filename, L->lineno, expect);
    switch (L->current.ID) {
        case INT:   fprintf(stderr, "integer\n");
                    break;
        case VAR:   fprintf(stderr, "variable\n");
                    break;
        case END:   fprintf(stderr, "end of file\n");
                    break;
        default:    fprintf(stderr, "%c\n", L->current.ID);
    }

    exit(1);
}

unsigned variable(lexer* L)
{
    if (L->current.ID  != VAR) {
        error(L, "variable");
    }
    unsigned vid = L->current.attrib;
    next_token(L);
    return vid;
}


pexpr term(lexer* L)
{
    pexpr e = 0;
    if (L->current.ID == INT) {
        e = int_expr(L->current);
        next_token(L);
        return e;
    }
    if (L->current.ID == VAR) {
        e = var_expr(L->current);
        next_token(L);
        return e;
    }
    if (L->current.ID == '-') {
        token optor = L->current;
        next_token(L);
        e = term(L);
        return unary_expr(optor, e);
    }
    if (L->current.ID == '(') {
        next_token(L);
        e = expression(L);
        if (L->current.ID != ')') error(L, ")");
        next_token(L);
        return e;
    }
    error(L, "integer or variable");
    return 0;
}

pexpr product(lexer* L)
{
    pexpr left = term(L);
    while (L->current.ID == '*' || L->current.ID == '/') {
        token op = L->current;
        next_token(L);
        pexpr right = term(L);
        left = binary_expr(left, op, right);
    }
    return left;
}

pexpr expression(lexer* L)
{
    pexpr left = product(L);
    while (L->current.ID == '+' || L->current.ID == '-') {
        token op = L->current;
        next_token(L);
        pexpr right = product(L);
        left = binary_expr(left, op, right);
    }
    return left;
}

pstmt statement(lexer* L)
{
    if (L->current.ID == END) {
        return 0;
    }
    unsigned v = variable(L);
    if (SEMI == L->current.ID) {
        pstmt s = print_stmt(v, L->current);
        next_token(L);
        return s;
    }
    if (GETS == L->current.ID) {
        next_token(L);
        pexpr rhs = expression(L);
        if (SEMI != L->current.ID) error(L, ";");
        pstmt s = assign_stmt(v, rhs, L->current);
        next_token(L);
        return s;
    }
    error(L, "= or ;");
    return 0;
}


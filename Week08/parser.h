#ifndef PARSER_H
#define PARSER_H

#include "lexer.h"
#include "ast.h"

// Returns
//  0 for end of file
//  1 for assignment, i.e,   var = E ;
//  2 for just the variable i.e. var ;
//
pstmt statement(lexer* L);

// E
pexpr expression(lexer* L);

// P
pexpr product(lexer* L);

// T
pexpr term(lexer* L);


#endif

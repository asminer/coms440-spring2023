
#include "ast.h"
#include <stdlib.h>

void* mymalloc(unsigned bytes)
{
    void* ptr = malloc(bytes);
    if (!ptr) {
        fprintf(stderr, "Bruh.  Malloc fail.  Bad vibe.\n");
        exit(2);
    }
    return ptr;
}

void type_error(pexpr left, token optor, pexpr right)
{
    fprintf(stderr, "Type error on line %u\n", optor->lineno);
    fprintf(stderr, "\t%s %c %s is not allowed\n",
                left->is_integer ? "int" : "real",
                optor.ID,
                right->is_integer ? "int" : "real");
    exit(1);
}

//
// expressions
//

pexpr int_expr(token value)
{
    pexpr e = mymalloc(sizeof(struct expr));
    e->is_integer = true;
    e->optor = value;
    e->left = 0;
    e->right = 0;
    return e;
}

pexpr var_expr(token var)
{
    pexpr e = mymalloc(sizeof(struct expr));
    e->is_integer = (var.attrib < 'p');
    e->optor = var;
    e->left = 0;
    e->right = 0;
    return e;
}

pexpr unary_expr(token optor, pexpr opnd)
{
    pexpr e = mymalloc(sizeof(struct expr));
    e->is_integer = opnd->is_integer;
    e->optor = optor;
    e->left = 0;
    e->right = opnd;
    return e;
}

pexpr binary_expr(pexpr left, token optor, pexpr right)
{
    pexpr e = mymalloc(sizeof(struct expr));
    if (left->is_integer != right->is_integer) {
        type_error(left, optor, right);
    }
    if (left->is_integer && right->is_integer) {
        e->is_integer = (optor->ID != SLASH);
    } else {
        e->is_integer = false;
    }
    e->optor = optor;
    e->left = left;
    e->right = right;
    return e;
}

//
// statements
//

pstmt assign_stmt(unsigned var, pexpr rhs, token semi)
{
    pstmt s = mymalloc(sizeof(struct stmt));
    bool var_is_integer = (var < 'p');
    if (var_is_integer != rhs->is_integer) {
        fprintf(stderr, "Type error on line %u\n", semi.lineno);
        fprintf(stderr, "\tAssignment type mismatch\n");
        exit(1);
    }
    s->var = var;
    s->rhs = rhs;
    s->lineno = semi.lineno;
    return s;
}

pstmt print_stmt(unsigned var, token semi)
{
    pstmt s = mymalloc(sizeof(struct stmt));
    s->var = var;
    s->rhs = 0;
    s->lineno = semi.lineno;
    return s;
}


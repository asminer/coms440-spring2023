
#include "lexer.h"

#include <stdlib.h>

void init_lexer(lexer *L, const char* fname)
{
    if (!L) return;     // Make sure L is not null
    L->filename = fname;
    L->infile = fopen(fname, "r");
    if (!L->infile) {
        fprintf(stderr, "Couldn't open file %s\n", fname);
        exit(1);
    }
    L->lineno = 1;
    next_token(L);
}

/*
    Set the current token of L to whatever the next token is on
    the input stream.
    Use END for eof or other error.
*/
void next_token(lexer *L)
{
    if (!L) return;

    if (!L->infile) {
        L->current.ID = END;
        return;
    }

    int c;
    char is_comment = 0;
    for (;;) {
        c = fgetc(L->infile);

        if (EOF == c) {
            L->current.ID = END;
            L->current.lineno = L->lineno;
            return;
        }

        if ((' ' == c)||('\t' == c)||('\r' == c)) {
            continue;
        }
        if ('\n' == c) {
            L->lineno++;
            is_comment = 0;
            continue;
        }

        if (is_comment) continue;

        if ('#' == c) {
            is_comment = 1;
            continue;
        }

        L->current.lineno = L->lineno;

        if ( ('(' == c) || (')' == c) || ('+' == c) || ('-' == c) ||
             ('*' == c) || ('/' == c) || (';' == c) || ('=' == c) )
        {
            L->current.ID = c;
            return;
        }

        if ( ('a' <= c) && (c <= 'z') )
        {
            L->current.ID = VAR;
            L->current.attrib = c;
            return;
        }

        if ( ('0' <= c) && (c <= '9') )
        {
            L->current.ID = INT;
            L->current.attrib = c - '0';
            for (;;) {
                c = fgetc(L->infile);
                if (( c < '0') || (c > '9')) {
                    // oops one too many!
                    ungetc(c, L->infile);
                    return;
                }
                L->current.attrib *= 10;
                L->current.attrib += c - '0';
            }
        }

        fprintf(stderr, "Unexpected character: %c\n", c);
        exit(1);

    } // for

}



%{
/*
    Build this with
        bison -d wimpy.y
*/

#include <stdio.h>

void init_lexer();
int yylex();

extern char* yytext;
extern int yylineno;

void yyerror(const char* msg)
{
    fprintf(stderr, "Bruh, on line like %d, you put '%s'. That's a problem.\n\t%s\n",
      yylineno, yytext, msg
    );
}

%}

%token PLUS MINUS STAR SLASH LPAR RPAR ASSIGN SEMI INT VAR

%left PLUS MINUS
%left STAR SLASH
%nonassoc UMINUS

%%

stmt : VAR ASSIGN expr SEMI
     | VAR SEMI
     ;

expr : expr PLUS expr
     | expr MINUS expr
     | expr STAR expr
     | expr SLASH expr
     | MINUS expr   %prec UMINUS
     | VAR
     | INT
     ;

%%

int main()
{
  init_lexer();
  return yyparse();
}


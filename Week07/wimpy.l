
%{
/*
  Build this with
      flex wimpy.l
*/
#include <stdio.h>
#include <stdlib.h>
#include "wimpy.tab.h"  // bison will build this

%}

%option noyywrap
%option yylineno

digit   [0-9]
space   [ \t\r\n]
letter  [a-z]

%%

{digit}+  { return INT; }
{letter}  { return VAR; }

"+"       { return PLUS; }
"-"       { return MINUS; }
"*"       { return STAR; }
"/"       { return SLASH; }
"="       { return ASSIGN; }
";"       { return SEMI; }
"("       { return LPAR; }
")"       { return RPAR; }

#.*[\n]   { /* ignore comment */ }
{space}*  { /* ignore whitespace */ }
.         { fprintf(stderr, "Unexpected char bruh: '%s'\n", yytext);
            exit(1);
          }

%%

void init_lexer()
{
  yylineno = 1;
}


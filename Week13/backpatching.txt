
Backpatching
======================================================================

Idea: convert inherited label attributes into synthesized attributes.
How?

Example: var decs.

  var_decl -> type varlist ;      { varlist.type = type.type }
  varlist -> varlist1 , var       { var.type = varlist.type;
                                    varlist1.type = varlist.type; }
  varlist -> var


To convert to synthesized:
have varlist build a list of identifiers, that need type information
(synthesized)

  var_decl -> type varlist ;      {  /* fill in type info into the list
                                        that varlist gives us */ }

Backpatching idea:
    we will do this with code generation
    we may have goto instructions with "blanks"
    and later we will fill in those blanks when we know the labels

    e.g. "if x > 3 goto __"


    Later: we will figure out that some instructions want to goto label L5
    and we plug that in to the __


* We will keep a list of instructions that need their goto labels filled in
  -> within a list, all of the instructions will have the same goto target

  -> once we know the location, we fill them in just for that list,
     and then destroy the list

  -> old inherited label attributes become "list of instruction" synthesized
      attributes

Note:
For incremental translation, this means either
  * buffer the instructions, so we can fill in goto targets later
    only flush buffer when all jump targets are known
    (e.g., at the end of a function)

  * write instructions to a file, then modify the file later
    when the jump targets are known


This is sort of like the "observer pattern"

The notes will implement this in a simple way, but not very elegant.
We could design a more elegant (encapsulated, etc) way to do this.

Assumptions for our semantic rules / implementation:

* Instructions are stored in an array buffer.
  (Assume the array is large enough.)

* Instruction labels will be the array index of the instruction
    i.e. the array slot holding the instruction

    This means our "list of instructions" will be lists of integers

* Global variable: "nextinstr" is the array index where the next instruction
    will be stored.

* Assume "output_instruction" adds the instruction into the array
    (into the nextinstr slot, and increments nextinstr)

Helpers:

* makelist(i) : create and return a new list containing only index i

* mergelists(p1, p2, ...) : merge lists p1, p2, ..., and return the merged list

* backpatch(p, i): go through list p, set jump target for all instructions
    in p, to the instruction at index i.  Destroy list p after that.


We need an updated grammar with more markers
  (These are hacks!  Productions to cause code to execute at a certain point.)

Production:             Semantic rules:
  M -> epsilon          { M.instr = nextinstr; }


So now onto the good stuff.  Start with boolean expressions.

Production:
    B -> true

Old rule:
    { output_instruction("goto ", t); }

Backpatching:
{
    B.truelist = makelist(
      output_instruction("goto  __")
    );
}

Production:
    B -> false

Backpatching:
{
    B.falselist = makelist(
      output_instruction("goto  __")
    );
}


Production:
    B -> E1 relop E2

Old rule:
    {
          E1.generate(...);
          E2.generate(...);
          output_instruction("if ", E1.addr, relop, E2.addr, " goto ", B.true);
          output_instruction("goto ", B.false);
    }

Backpatching:
    {
          // E1.generate(...); already generated
          // E2.generate(...); already generated
          B.truelist = makelist(
            output_instruction("if ", E1.addr, relop, E2.addr, " goto __")
          );
          B.falselist = makelist(
            output_instruction("goto __")
          );
    }


Production:
    B -> ! B1

Old rule:
    {
        B1.generate(f, t);
    }

Backpatching:
    {
        B.truelist = B1.falselist;
        B.falselist = B1.truelist;
    }



Production:
    B -> B1 || M B2
               ^
              let us remember the first instruction location in B2

Old:
    {
        label mid = new label;
        B1.generate(t, mid);
        output_label(mid);
        B2.generate(t, f);
    }

Backpatching:
    {
        // B1, B2 already generated
        B.truelist = merge_lists(
            B1.truelist, B2.truelist
        );
        B.falselist = B2.falselist;

        // We know where B1.false should jump to now so fill that in
        backpatch(
            B1.falselist, M.instr
        );
    }


Production:
    B -> B1 && M B2

Backpatching:
    {
        // B1, B2 already generated
        B.truelist = B2.truelist;
        B.falselist = merge_lists(
            B1.falselist, B2.falselist
        );

        // We know where B1.true should jump to now so fill that in
        backpatch(
            B1.truelist, M.instr
        );
    }


What about control flow statements?

Let L denote List of Statements

Production:

    L -> L1 M S
            ^
            So we can "jump to next thing" in L1 if needed

Backpatching:
    {
        L.nextlist = S.nextlist;
        backpatch(L1.nextlist, M.instr);
    }


Production:

    L -> S

Backpatching:
    {
        L.nextlist = S.nextlist;
    }

Production:
    S -> { L }

Backpatching:
    {
        S.nextlist = L.nextlist;
    }


Production:
    S -> if ( B ) M S1
                  ^
                  So we know where B should jump if it's true

Old:
generate(label next)
{
    label M = new label;
    B.generate(M, next);
    output_label(M);
    S1.generate(next);
}

Backpatching:
{
    // B, S1 code already generated
    backpatch(B.truelist, M.instr);
    S.nextlist = merge( B.falselist, S1.nextlist );
}


For if-then-else, we need another marker!

Production:               Rule:
      N -> epsilon        {  N.nextlist = makelist( output_instr("goto __")) }


Production:
      S -> if ( B ) M1 S1 N else M2 S2

Old:
generate(label next)
{
    label tp = new label;
    label fp = new label;
    B.generate(tp, fp);
    output_label(tp);
    S1.generate(next);
    output_instruction("goto", next);
    output_label(fp);
    S2.generate(next);
}

Backpatching:
{
    // B, M, S1, N, M2, S2 already generated

    backpatch(B.truelist, M1.instr);
    backpatch(B.falselist, M2.instr);
    S.nextlist = mergelist(S1.nextlist, N.nextlist, S2.nextlist);
}


Production:

S -> while  M1  ( B ) M2 S1

Old:
generate(label next)
{
    label loop = new label;
    label tp   = new label;
    output_label(loop);
    B.generate(tp, next);
    output_label(tp);
    S1.generate(loop);
    output_instruction("goto " , loop);
}


Backpatching:
{
    backpatch(S1.nextlist, M1.instr);
    backpatch(B.truelist,  M2.instr);
    S.nextlist(B.falselist);
    output_instruction("goto ", M1.instr);
}


Huge example to see sort of how this works


Source code:

    if ((x>15) || ( (y<0) && (z<0) ) )
        func1();
    else
        func2();


AST that's relevant (with markers)


                   (13)S
        _______________|_________________________________
       /       |       |     \       \    \       \      \
     if  (7) ( B )  (8)M  (9) S1 (10) N  else  (11)M  (12)S2
         ____/ | \_________     \________                 |
        /     /     \      \             \                |
       /     ||  (2) M   (6)B2            \               |
      /                 __/ | \____        \              |
     /                 /   /       \        \             |
    /                 /   /         \        \            |
(1) B1         (3)  B3   &&  (4)M  (5)B4      E           E
   /|\             /|\              /|\        \          |
  / | \           / | \            / | \        |         |
 /  |  \         /  |  \          /  |  \     fcall     fcall
E1  >  E2       E3  <  E4        E5  <  E6      |         |
|      |        |      |         |      |       |         |
|      |        |      |         |      |     func1()   func2()
id     val      id     val       id     val
|      |        |      |         |      |
|      |        |      |         |      |
x      15       y      0         z      0


Processing at each node from (1) to (13)
----------------------------------------------------------------------
(1) B1 -> E1 relop E2
    {
       B1.truelist = makelist(
            output_instr("if" , E1.addr, >, E2.addr, "goto __")
       );
       B1.falselist = makelist(
            output_instr("goto __")
       );
    }

  B1.truelist  = 42
  B1.falselist = 43

(2) M -> epsilon
    {
        M.instr = 44
    }

(3) B3 -> E3 relop E4
    {
       B3.truelist = makelist(
            output_instr("if" , E3.addr, <, E4.addr, "goto __")
       );
       B3.falselist = makelist(
            output_instr("goto __")
       );
    }

    B3.truelist  = 44
    B3.falselist = 45

(4) M -> epsilon
    {
        M.instr = 46
    }

(5) B4 -> E5 relop E6
    {
       B4.truelist = makelist(
            output_instr("if" , E5.addr, <, E6.addr, "goto __")
       );
       B4.falselist = makelist(
            output_instr("goto __")
       );
    }

    B4.truelist  = 46
    B4.falselist = 47

(6) B2 -> B3 && (4)M B4
    {
        B2.truelist = B4.truelist
        B2.falselist = mergelists(B3.falselist, B4.falselist);
        backpatch(B3.truelist, (4)M.instr);
    }                 ^         ^
                      44        46
    B2.truelist:  46
    B2.falselist: 45, 47

(7) B -> B1 || (2)M B2
    {
        B.truelist = mergelist(B1.truelist, B2.truelist);
        B.falselist = B2.falselist;
        backpatch(B1.falselist, (2)M.instr)
                      ^           ^
                      43          44
    }

    B.truelist = 42, 46
    B.falselist = 45, 47

(8) M -> epsilon
    {
        M.instr = 48
    }

(9)

(10) N -> epsilon
    {
        N.nextlist = makelist(output_instr("goto __"));
    }
    N.nextlist = 49

(11) M -> epsilon
    {
        M.instr = 50
    }

(12)
     S -> if  (7) ( B )  (8)M  (9) S1 (10) N  else  (11)M  (12)S2
    {
        backpatch(B.truelist, (8)M.instr)
                  ^           ^
                  42, 46      48

        backpatch(B.falselist, (11)M.instr)
                  ^             ^
                  45, 47        50

        S.nextlist = mergelists(S1.nextlist, (10) N.nextlist, S2.nextlist)
                                ^                 ^            ^
                                null              49           null
    }
    S.nextlist = 49

Instruction array:
----------------------------------------------------------------------

     ...
42:  if x > 15 goto 48
43:  goto 44
44:  if y < 0 goto 46
45:  goto 50
46:  if z < 0 goto 48
47:  goto 50
48:  call func1, 0
49:  goto 51
50:  call func2, 0
51:


    if ((x>15) || ( (y<0) && (z<0) ) )
        func1();
    else
        func2();

Yay it worked!
But note:

43:  goto 44
44:  if y < 0 goto 46


break and continue
======================================================================
these are used within loops

  break: jumps out of the innermost loop

  continue: go to start of the innermost loop

      while (...) {
          ..
          if (thingy) break;      // break out of the loop
          ..            ^
      }                 |
                      this statement will cause execution
       <------------- to jump to here.



Both of these are just "goto __" statements
      break: goto next instruction after the loop
                  ^
                  we already have this (sort of)! so use it

      continue: goto 'next loop iteration'
                  ^
                  we don't have this


So for statements, we need to track 3 labels!
      next
      where to jump on break
      where to jump on continue

Notice:
      labels will be bogus values until we enter a loop
      -> if we see a break with bogus value -> error, not in a loop


Food for thought:

How to lay out code for a "for loop"?

for( init; condition; update )
{
}

I could use:

init;
while (condition) {
    ...
    update;
}

I could also use:

init
goto HERE
do {
  ...
  update
  HERE:
} while condition


for (;;)
{
    ...
    if (c == EOF) break;
}



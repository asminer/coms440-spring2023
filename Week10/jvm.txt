The Java Virtual Machine
============================================================
* Abstract machine that can execute Java bytecode
* Will read and execute (and check and other stuff) class files
* Is a 32-bit stack machine (except for local vars)
    -> each "stack slot" is 32 bits
    -> long and double types consume 2 stack slots
    -> int and float are 32-bits (one slot)
    -> bool, char, byte, short are operated on as 32-bit integers
       (probably arrays of chars are "compacted")


Frames
------------------------------------------------------------
* Memory space used for method calls
* A new frame is created for each method call
* The frame is destroyed when the method exits
* A frame contains:
    - An array of local variable slots, numbered from 0
      Parameters are included here.

    - An operand stack for performing computations



Some of the many many Java bytecode instructions
============================================================

For a full list:
https://en.wikipedia.org/wiki/List_of_Java_bytecode_instructions


Binary arithmetic ops:
------------------------------------------------------------

    Tadd: +
    Tsub: -
    Tmul: *
    Tdiv: /
    Trem: %

Where T is the type of the operands, one of:
    i: integer
    c: char
    f: float
    a: reference (array)

For arithmetic: T will be i or f for us.

    iadd: this will pop two items from the stack, and pushes their sum
          (as integers!)

          before: stack is  ..., value1, value2
          after : stack is  ..., value1+value2


Unary ops:
------------------------------------------------------------

  Tneg: unary -

      e.g.   fneg:   before stack is: ...., value
                     after  stack is:  ..., -value


Load and store ops
------------------------------------------------------------

    Tload index     : push value in local variable slot # index
                      onto the stack

    Special cases:

      for indexes 0-3, use instead

    Tload_index

      For example to grab integer variables at various slots use:
            iload_0
            iload_1
            iload_2
            iload_3
            iload 4
            iload 5


    Tstore index    : pop value off the stack and store it in local
                      variable #index

    Special cases: for indexes 0-3, use instead

    Tstore_index

      For example to save a whole bunch of stuff into float variables:
            fstore_0
            fstore_3
            fstore 4
            fstore 5


Constants
------------------------------------------------------------

    fconst_0    : push float 0.0
    fconst_1    : push float 1.0
    fconst_2    : push float 2.0

    iconst_m1   : push integer -1
    iconst_0    : push 0
    iconst_1    : push 1
    ...
    iconst_5    : push 5

    bipush byte   : push a byte   (range -128..127)
    sipush short  : push a short  (range -32768..32767)

    ldc val     : load a constant value

    for example:
        ldc 3.14159f
        ldc 98765432


Array load and store
------------------------------------------------------------
                        stack before          stack after
  Taload            ...., array, index        ...., array[index]
  ^
  type of the array

  Tastore           ...., array, index, value     .....
  ^
  will set array[index] = value



"Global variables"
=
Static field load and store
------------------------------------------------------------

getstatic Field (classname) (fieldname) (type)    -> push onto stack

putstatic Field (classname) (fieldname) (type)    -> pop off stack and store
                                          ^
                                          these types are different
                                          will discuss in a second...
  for example:
      foo.java
          class foo {
              static int thingy;
          }

      foo.j
          ....
          getstatic Field foo thingy I



Calling methods
------------------------------------------------------------

invokestatic Method [classname] [methodname]  (params)
                                                ^
                                                also later

      ^
   will pop off items as needed for paramters,
  call function, and any return value will be pushed onto the stack


Return:
------------------------------------------------------------

    return            : just return don't return a value

    Treturn           : pop from stack and return that value
                        of type T = i, f


Misc stack manipulation
------------------------------------------------------------

instruction       stack before        stack after
-----------       -------------       --------------
swap              ..., v2, v1         ...., v1, v2

pop               ..., v1             ....,

dup               ..., v1             ...., v1, v1

dup_x1            ..., v2, v1         ...., v1, v2, v1

dup_x2            ..., v3, v2, v1     ....., v1, v3, v2, v1


Random conversions
------------------------------------------------------------

f2i         - float to int
i2f         - int to float
i2c         - int to char


Other random useful? instructions
------------------------------------------------------------

iinc  (local variable#) (delta)


Conditionals
------------------------------------------------------------
ifeq, ifne, ifge, ifgt, ifle, iflt

    consume top of stack (assume integer)
      if the integer is =, !=, >=, >, <=, < 0
      then jump to specified label


if_icmp*
      * is one of eq, ne, ge, gt, le, lt

      for example if_icmple

      top of stack: .., value1, value2 (integers)

      these are popped and compared and then we jump based on the comparison


What about float/double comparisons?
------------------------------------------------------------
  d/fcmpg       ..., value1, value2             ..., integer compare
  d/fcmpl       ..., value1, value2             ..., integer compare

compare two floats, store result on stack

    g:  1 on NAN
    l:  -1 on NAN


------------------------------------------------------------
Example
------------------------------------------------------------

Source code:
------------------------------------------------------------
do {
  i = i + 1;
} while (a[i] < v);


Assume
      i integer in variable slot 3
      v double  in variable slot 1
      a double[] in variable slot 0

stack
------------------------------------------------------------
  L:
      push i        // load i
      push 1        // get 1 on the stack
      +
      pop i         // store result in i
      push a        // stack is: a
      push i        // stack is: a, i
      push 8        // stack is: a, i, 8
      *             // stack is: a, i*8
      []            // stack is: a[i*8]
      push v        // stack is: a[i*8], v
      if < goto L

JVM:
------------------------------------------------------------
  L:
      iload_3       // load i
      iconst_1      // load 1
      iadd          // top of stack: i+1
      istore_3      // store in i
      aload_0       // load a
      iload_3       // load i
      daload        // top of stack is a[i]
      dload_1       // push v onto stack
      dcmpg         // compare a[i] and v
      iflt L


types in Java assembly
------------------------------------------------------------
used for getstatic/putstatic, and for method call types,
and for method declaration

      V:    void
      C:    char
      I:    int
      F:    float

[ = "array of"

      [C

objects:
    Lobject-name;


Examples:

    Read from static var classname.fld of type int and store on stack:

    getstatic Field classname fld I

    getstatic Field classname fld [I

calling and specifying a method:

    give the parameter types in order within ( ) with no spaces,
    then give the return type after the right )

static method void foo(char c[], int N)

  we call this using:

    invokestatic Method classname foo ([CI)V


Some misc. Java assembly stuff
------------------------------------------------------------

Declare the class name and parent class

    .class public (classname)
    .super java/lang/Object


Global variable:

    .field public static (varname) (type)


Method blocks:
------------------------------------------------------------
.method public static (name) : (type signature thingy)returntype
    .code stack N locals M
        ; instructions

    .end code
.end method

What are N and M?

    N: number of stack slots needed!
    M: number of local variable slots to use
        (includes parameters, which are numbered first)




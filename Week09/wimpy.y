
%{
/*
    Build this with
        bison -d wimpy.y
*/

#include <stdio.h>

void init_lexer();
int yylex();

extern char* yytext;
extern int yylineno;

void yyerror(const char* msg)
{
    fprintf(stderr, "Bruh, on line like %d, you put '%s'. That's a problem.\n\t%s\n",
      yylineno, yytext, msg
    );
}

long variable[26];  //what's the value of a variable?
char defined[26];   //was the variable assigned to yet?

long get_var(char var)
{
    if (!defined[var - 'a'])  {
        fprintf(stderr, "Bruh, variable %c not defined yet\n", var);
    }
    return variable[var - 'a'];
}

void set_var(char var, long value)
{
  variable[var - 'a'] = value;
  defined[var - 'a'] = 1;
}

void init_vars()
{
  int i;
  for(i=0; i<26; i++) defined[i] = 0;
}

%}

%union {
    long number;
    char varID;
}

%token PLUS MINUS STAR SLASH LPAR RPAR ASSIGN SEMI
%token <number> INT
%token <varID>  VAR

%type <number> expr

%left PLUS MINUS
%left STAR SLASH
%nonassoc UMINUS

%%

stmtlist: stmtlist stmt
        | stmt
        ;

stmt : VAR ASSIGN expr SEMI {  set_var($1, $3); }
     | VAR SEMI             {  printf("Variable %c is %ld\n", $1, get_var($1)); }
     ;

expr : expr PLUS expr           {   $$ = $1 + $3; }
     | expr MINUS expr          {   $$ = $1 - $3; }
     | expr STAR expr           {   $$ = $1 * $3; }
     | expr SLASH expr          {   $$ = $1 / $3; }
     | MINUS expr   %prec UMINUS    { $$ = - $2; }
     | VAR                      { $$ = get_var($1); }
     | INT                      { $$ = $1; }
     ;

%%

int main()
{
  init_lexer();
  init_vars();
  return yyparse();
}


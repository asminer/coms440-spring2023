
#ifndef LEXER_H
#define LEXER_H

#include <stdio.h>

#define END         0
#define INT         300
#define VAR         301
#define PLUS        '+'
#define MINUS       '-'
#define STAR        '*'
#define SLASH       '/'
#define LPAR        '('
#define RPAR        ')'
#define GETS        '='
#define SEMI        ';'

typedef struct {
    unsigned ID;        // token ID
    unsigned attrib;    // for INT and VAR
    unsigned lineno;
} token;

typedef struct {
    const char* filename;
    unsigned lineno;
    FILE* infile;
    token current;
} lexer;

void init_lexer(lexer *L, const char* fname);

void next_token(lexer *L);

#endif

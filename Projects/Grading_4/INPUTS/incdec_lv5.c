
void test1(int a)
{
    a++;
    return;
}

void test2(float x)
{
    x++;
    return;
}

void test3(int a)
{
    int b;
    b++;
    return;
}

void test4(float x)
{
    float y;
    y++;
    return;
}

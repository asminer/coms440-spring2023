
int test1(int a)
{
    return --a;
}

float test2(float x)
{
    return --x;
}

int test3(int a)
{
    int b;
    return --b;
}

float test4(float x)
{
    float y;
    return --y;
}

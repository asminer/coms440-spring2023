
int a, b;
float c, d;

void test1(int b)
{
  a -= b;
  return;
}

void test2(float d)
{
  c += d;
  return;
}

void test3(int a)
{
  b *= a;
  return;
}

void test4(float c)
{
  d /= c;
  return;
}

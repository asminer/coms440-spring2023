
within print4
    NAME read a
    MATCH getstatic Field expr_g1 a C

    NAME read b
    MATCH getstatic Field expr_g1 b C

    NAME read c
    MATCH getstatic Field expr_g1 c C
done

define wra1 {
    72
    putstatic Field expr_g1 a C
}
define wrb1 {
    101
    putstatic Field expr_g1 b C
}
define wrc1 {
    108
    putstatic Field expr_g1 c C
}

define wra2 {
    111
    putstatic Field expr_g1 a C
}
define wrb2 {
    32
    putstatic Field expr_g1 b C
}
define wrc2 {
    87
    putstatic Field expr_g1 c C
}

within main
    NAME write a
    MATCH wra1

    NAME write a
    MATCH wra2

    NAME write b
    MATCH wrb1

    NAME write b
    MATCH wrb2

    NAME write c
    MATCH wrc1

    NAME write c
    MATCH wrc2
done


within print4
    NAME read i
    MATCH getstatic Field expr_g2 i I

    NAME read j
    MATCH getstatic Field expr_g2 j I

    NAME read k
    MATCH getstatic Field expr_g2 k I
done

define wra1 {
    72
    putstatic Field expr_g2 i I
}
define wrb1 {
    101
    putstatic Field expr_g2 j I
}
define wrc1 {
    108
    putstatic Field expr_g2 k I
}

define wra2 {
    111
    putstatic Field expr_g2 i I
}
define wrb2 {
    32
    putstatic Field expr_g2 j I
}
define wrc2 {
    87
    putstatic Field expr_g2 k I
}

within main
    NAME write i
    MATCH wra1

    NAME write i
    MATCH wra2

    NAME write j
    MATCH wrb1

    NAME write j
    MATCH wrb2

    NAME write k
    MATCH wrc1

    NAME write k
    MATCH wrc2
done


within main
  NAME String literal
  MATCH ldc ['"]Hello, world
  NAME Java to C
  MATCH invokestatic [Mm]ethod lib440 java2c
  NAME putstring
  MATCH invokestatic [Mm]ethod lib440 putstring
  NAME return
  MATCH ireturn
done

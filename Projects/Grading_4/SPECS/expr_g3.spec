
within print4
    NAME read x
    MATCH getstatic Field expr_g3 x F

    NAME read y
    MATCH getstatic Field expr_g3 y F

    NAME read z
    MATCH getstatic Field expr_g3 z F
done

define wra1 {
    8[.]486
    putstatic Field expr_g3 x F
}
define wrb1 {
    10[.]05
    putstatic Field expr_g3 y F
}
define wrc1 {
    10[.]4
    putstatic Field expr_g3 z F
}

define wra2 {
    10[.]54
    putstatic Field expr_g3 x F
}
define wrb2 {
    5[.]66
    putstatic Field expr_g3 y F
}
define wrc2 {
    9[.]33
    putstatic Field expr_g3 z F
}

within main
    NAME write x
    MATCH wra1

    NAME write x
    MATCH wra2

    NAME write y
    MATCH wrb1

    NAME write y
    MATCH wrb2

    NAME write z
    MATCH wrc1

    NAME write z
    MATCH wrc2
done

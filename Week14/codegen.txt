
A simple code generator (8.6 in dragon)
======================================================================

Want:
    Algorithm to generate target code for a single basic block,
    that keeps track of registers to avoid unnecessary loads / stores.


Main issue: efficient use of registers

Registers are used for:
  (1) some or all operands for an instruction x = y <op> z
      must be registers
  (2) Registers are a good choice for compiler temporaries
  (3) Registers can be used for (global or local) values
      computed and used in basic blocks
  (4) Registers are often used to help with runtime storage management
      (e.g. managing runtime stack)

We assume there are some general-purpose registers that we may use

We will assume instructions of the form

  * LD reg, memory (load operation)
  * ST memory, reg (store)
  * OP regA, regB, regC
        computes regA = regB OP regC


Descriptors
=============================================================
Tables to keep track of what is currently stored where

Register descriptors
------------------------------------------------------------
For each register, keep track of which variable(s) whose
  current value is stored in the register.

Address descriptors
------------------------------------------------------------
For each program variable (or compiler temp etc.), keep track
of location(s) where the current value may be found.
    ^
    (registers, memory address, stack location)

Could be stored in the symbol table


"Obviously", we need to update the descriptors as we generate instructions.


Assume we have a function, getReg(I), that will select registers
for each memory location in the three-address instruction I.

I.e., we might ask:

      getReg(x = y + x)

and it will give us a register for x, and a register for y.
Based on descriptors, maybe other info like flow graph, etc.

Will discuss how to implement this later.


Plan for lecture(s):
  (1) how to generate instructions
  (2) how to update descriptors
  (3) how to end the basic block
  (4) a simple getReg implementation

======================================================================

How to generate instructions

For a generic 3-address instruction x = y <op> z, do the following:

(1) Use getReg(x = y <op> z) to obtain suggestions for registers
        Rx, Ry, Rz
        Rx: which register should we use for x

(2) Check register descriptor for Ry
        If y is not currently in Ry, issue instruction:
              LD Ry, y'
        Where y' is one of the memory addresses (found in address descriptor
        for y) that currently holds y.

        (and update descriptors, but we discuss that next)


(3) Same for Rz

(4) Issue instruction

          OP Rx, Ry, Rz


For a copy instruction x = y, do the following:

(1) Use getReg(x=y), which will give the same registers for Rx and Ry
    (i.e., Rx is the same as Ry)

(2) Same as step (2) above

(3) There is no step 3 (no z operand)

(4) Don't issue any instruction
      because we instead are managing descriptors well


======================================================================

How to update descriptors

(1)  Whenever we issue a load instruction LD R, x
      (a) change register descriptor for R: holds only x
      (b) change address descriptor for x:  add R as a location

(2)  Whenever we issue a store instruction ST x, R
      (a) change address descriptor for x: add memory location x for x

(3)  When we issue instruction OP Rx, Ry, Rz
      (does Rx = Ry OP Rz, implementing x = y <op> z)

     (a) change register descriptor for Rx: holds only x
     (b) Change address descriptor for x so its only location is Rx
         (this includes, removing address for x)

(4)  When we process a copy x = y
      after dealing with the load of y into Ry (step 1) if it was
      needed
      (a) Add x to register descriptor for Ry
      (b) Change address descriptor for x so its only location is Ry


Take care of the following:

  Whenever we remove a variable z from a register descriptor R,
  we also must remove register R as a storage location for variable z.

  Whenever we remove a register R as a storage location for variable z,
  we also must remove variable z from the register descriptor for R.


======================================================================

How to end the block

  We need to store anything that needs to be stored.

  Any variable whose only storage location is a register,
  (potentially) must be stored.

  But we don't need to store variables that are not used later
  (e.g., compiler temporaries)

  "Algorithm":

  For each block variable x that is "live on exit" (might be used later)
      check address descriptor for x
          if location is only some register(s) R, then issue instruction
                ST x, R

======================================================================

Example

Block:
    t1 = a - b
    t2 = a - c
    t3 = t1 + t2
    a = d
    d = t3 + t2

Notes:
    * t1, t2, t3 are temporaries, not used outside of the block
    * a, b, c, d are variables that are live on exit
    * getReg() is magic for now

We need to keep track of the descriptors:

register descriptors:
      R1: d
      R2: a
      R3: t3

address descriptors:
       a:  R2
       b:  b
       c:  c
       d:  R1
      t1: R2
      t2:
      t3: R3


3-addr        getReg        code we generate        Descriptor updates
----------    ----------    --------------------    ------------------------
t1 = a - b     a: R1          LD R1, a                R1: a
               b: R2          LD R2, b                R2: b
              t1: R2          SUB R2, R1, R2          t1: R2

t2 = a - c     a: R1
               c: R3          LD R3, c                R3: c
              t2: R1          SUB R1, R1, R3          R1: t2, t2: R1

t3 = t1+t2    t1: R2
              t2: R1
              t3: R3          ADD R3, R2, R1          R3: r3, t3: R3

a = d          d: R2          LD R2, d                R2 = d, a
                                                      a: R2, d += R2

d = t3+t2     t3: R3          ADD R1, R3, R1          R1: d
              t2: R1                                  R2: a
               d: R1

end block

  Store instructions are needed for a and d

                              ST a, R2
                              ST d, R1

So we get:

LD R1, a
LD R2, b
SUB R2, R1, R2
LD R3, c
SUB R1, R1, R3
ADD R3, R2, R1
LD R2, d
ADD R1, R3, R1
ST a, R2
ST d, R1


Designing getReg()
======================================================================
Here's a simple algorithm for getReg()

Suppose the instruction is x = y <op> z

(1) Select a register Ry for y using the following rules.

    (i)   If y is already in some register, pick one and use that

    (ii)  If y is not in some register, and there is an "empty"
          register, pick one of those

    (iii) If y is not in some register, and all registers are in use
          We need to re-use some register

          Consider a candidate register R, and let v be a variable
          held by R.

          (a) If address descriptor says v is somewhere else besides R,
              don't need to store v.

          (b) If v is x (lhs of the instruction)
              and x is not an operand in rhs of the instruction
              don't need to store v and we don't need to use v.

          (c) If v is not used later (in or after this instruction)
              in the "live" sense, don't need to store v

          (d) Otherwise, if cases (a) - (c) don't apply,
              then we must store the value back in v by generating
              a store instruction

                      ST v, R

          Count the number of stores required for each candidate register R.
          Select one requiring the fewest # of stores.

          This is just a greedy HEURISTIC.


(2) Select a register Rz for z using the same rules as (1)

(3) Select a register Rx for x using similar rules except:

        (i) if x is currently in a register, *AND* it is the
            only variable in that register, use it

        (ii) same as before

        (iii) same as before

        (iv) If variable y is not used after this instruction (in the
              live sense), we can use Ry as Rx

             If variable z is not used .... we can use Rz as Rx








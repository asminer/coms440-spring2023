
Translation and control flow
----------------------------------------------------------------------

How do we translate boolean expressions like &&, ||, ! ?

Simple approach: do the same thing for expressions, e.g.,
generate code for left, generate code for right, then
add an instruction to do the and of the two.

Problem: in most languages (C, C++, Java, Python), boolean operators
"short circuit"

this means: don't evaluate the rest of a boolean expression once
          its value is known

          false && x = false

          true || x = true


C specifies that boolean expressions
      (1) evaluate in order
      (2) short circuit

Programmers use this all the time!!!

    if ( ptr && ptr[0] < 7) ...

          ^ if ptr is null, then ptr[0] will barf so
      this relies on short circuiting!


Let's do an example expression by hand:

    t = (x>15) || ( (y<0) && (z<0) )

Assuming && has higher precedence than ||
with short-circuiting this code should translate to something like:

    if x > 15 goto yes
    if y >= 0 goto no
    if z >= 0 goto no
yes:
    t = true
    goto next
no:
    t = false
next:


As the example shows, we will need the following attributes for
short-circuiting boolean expressions / statements:

    * true part: a label to jump to when the condition is true
    * false part: a label to jump to when the condition is false
    * next: label for the next statement

"Bad news": these attributes will be inherited, not synthesized.

Note: we don't really need to store the result of the boolean expression,
  we can do everything based on "where we jump to".


Code generation! woo-hoo!
------------------------------------------------------------

Consider the simple conditional:

  Production:
  stmt ->   if ( B )  stmt1 ;

note: stmt1 can be a block of statements or just one.

Before semantic rules, let's map out the code:

        (code to compute B with short circuiting)
    B.true:
        (code for stmt1)
    B.false:
    B.next:
        (whatever intructions are after this)


How to write the semantic rules?
Book:

{
    B.true = new_label();
    B.false = stmt.next;      // parent!
    stmt1.next = stmt.next;   // inherited attribute
    stmt.code = concatenate(
        B.code,
        label(B.true),      // generate the label for B.true
        stmt1.code
    );
}


How to do incremental translation?

Method 1: (book)
-------------------
    add a "marker" to the production
        -> grammar symbol that produces epsilon
        -> used only to generate code where we want

In this case:

Production:                   Sem rules:

    L -> epsilon            { output_label(L.label); }
                                            ^ still inherited

    S -> if ( B ) L S1
                            { B.true = new_label();
                              L.label = B.true;
                              B.false = S.next;
                              S1.next = S.next;
                            }


Method 2: (Miner)
--------------------
    Traverse the AST/Parse tree

    Assume we have an object for each parse tree node
    and we can call a generate() method to generate code.

        You can think of this as a generate() method for each production.

    We can pass the inherited attributes as parameters to generate()
    and the synthesized ones are return values from generate()


Assume for booleans we have

      B.generate(truepart, falsepart)

      S.generate(next)

So then the semantic rules:

    stmt -> if ( B ) stmt1 ;

void generate(label next)
{
    label truept = new_label();
    B.generate(truept, next);
    output_label(truept);
    stmt1.generate(next);
}


------------------------------------------------------------

  Production:
  S ->   if ( B )  S1 ; else S2 ;

Before semantic rules, let's map out the code:

        (code to compute B with short circuiting)
    B.true:
        (code for s1)
        jump to B.next
    B.false:
        (code for s2)
    B.next:
        (whatever intructions are after this)


S.generate(label next)
{
    label truept = new_label();
    label falsept = new_label();
    B.generate(truept, falsept);
    output_label(truept);
    S1.generate(next);
    output_instruction("goto ", next);
    output_label(falsept);
    S2.generate(next);
//
//  assume S's next label is generated elsewhere
//
}

------------------------------------------------------------

  Production:
  S ->   while ( B )  S1 ;

    loop:
        (code to compute B with short circuiting)
    B.true:
        (code for stmt1)
        goto loop
    B.false:
    next:
        (whatever intructions are after this)


S.generate(label next)
{
    label loop = new_label();
    label tpart = new_label();
    output_label(loop);
    B.generate(tpart, next);
    output_label(tpart);
    S1.generate(loop);
    output_instruction("goto ", loop);
}


------------------------------------------------------------

  Production:
  S ->  S1 S2


  Desired code:

      (code for s1)
  S1.next:
      (code for s2)


  S.generate(label next)
  {
      label middle = new_label();
      S1.generate(middle);
      output_label(middle);
      S2.generate(next);
  }

Discussion:
  If we are concerned about being wasteful of labels
  we could track within a label object "are there any instructions
  that jump to me", and if so, output_label will print it,
  otherwise not.

  Or, we might not care.


What about the ternary operator?
------------------------------------------------------------

Production:
    E -> B ? E1 : E2

Like if then else but we need the address of where the expression
result is stored (at least for 3 address code)

Desired code:

      (code for B)
  B.true:
      (code for E1)
      t = E1.addr
      goto next
  B.false:
      (code for E2)
      t = E2.addr
  next:


This means that expressions should also have an inherited "next" label
(or we can fake it)

Code generation (Miner's way) (3-address)

  E.generate(label next)
  {
      label tpart = new_label();
      label fpart = new_label();
      E.addr = new_temporary();
      B.generate(tpart, fpart);
      output_label(tpart);
      E1.generate(next);
      //          ^ not sure about this?
      output_instruction(t, " = ", E1.addr);
      output_instruction("goto ", next);
      output_label(fpart);
      E2.generate(next);
      output_instruction(t, " = ", E2.addr);
  }

What about stack machines?
Now no temporaries but otherwise the same code:

  E.generate(label next)
  {
      label tpart = new_label();
      label fpart = new_label();
      B.generate(tpart, fpart);
      output_label(tpart);
      E1.generate(next);
      //          ^ not sure about this?
      output_instruction("goto ", next);
      output_label(fpart);
      E2.generate(next);
  }

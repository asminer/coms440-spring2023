
#include "lexer.h"

int main(int argc, const char** argv)
{
    if (argc != 2) {
        fprintf(stderr, "Bruh.\n");
        return 1;
    }
    lexer L;
    init_lexer(&L, argv[1]);
    while (L.current.ID) {
        printf("On line %u got token %u (%c), attribute %u\n",
                L.current.lineno, L.current.ID, L.current.ID, L.current.attrib);
        next_token(&L);
    }
    return 0;
}

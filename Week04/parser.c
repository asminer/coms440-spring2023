
#include "parser.h"

#include <stdlib.h>

void error(lexer* L, const char* expect)
{
    fprintf(stderr, "Syntax error file %s line %u:\n\tExpected %s, saw ",
                L->filename, L->lineno, expect);
    switch (L->current.ID) {
        case INT:   fprintf(stderr, "integer\n");
                    break;
        case VAR:   fprintf(stderr, "variable\n");
                    break;
        case END:   fprintf(stderr, "end of file\n");
                    break;
        default:    fprintf(stderr, "%c\n", L->current.ID);
    }

    exit(1);
}

void variable(lexer* L)
{
    if (L->current.ID  != VAR) {
        error(L, "variable");
    }
    next_token(L);
}

void term(lexer* L)
{
    if (L->current.ID == INT) {
        next_token(L);
        return;
    }
    if (L->current.ID == VAR) {
        variable(L);
        return;
    }
    if (L->current.ID == '-') {
        next_token(L);
        term(L);
        return;
    }
    if (L->current.ID == '(') {
        next_token(L);
        expression(L);
        if (L->current.ID != ')') error(L, ")");
        next_token(L);
        return;
    }
    error(L, "integer or variable");
}

void product(lexer* L)
{
    term(L);
    while (L->current.ID == '*' || L->current.ID == '/') {
        next_token(L);
        term(L);
    }
}

void expression(lexer* L)
{
    product(L);
    while (L->current.ID == '+' || L->current.ID == '-') {
        next_token(L);
        product(L);
    }
}

int statement(lexer* L)
{
    if (L->current.ID == END) {
        return 0;
    }
    variable(L);
    if (SEMI == L->current.ID) {
        next_token(L);
        return 2;
    }
    if (GETS == L->current.ID) {
        next_token(L);
        expression(L);
        if (SEMI != L->current.ID) error(L, ";");
        next_token(L);
        return 1;
    }
    error(L, "= or ;");
    return 0;
}


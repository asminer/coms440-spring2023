
#ifndef PARSER_H
#define PARSER_H

#include "lexer.h"

// Returns
//  0 for end of file
//  1 for assignment, i.e,   var = E ;
//  2 for just the variable i.e. var ;
//
int statement(lexer* L);

// E
void expression(lexer* L);

// P
void product(lexer* L);

// T
void term(lexer* L);


#endif

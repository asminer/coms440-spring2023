
%{
#include <stdio.h>
%}

%option noyywrap

digit     [0-9]
space     [ \t\r\n]
letter    [a-z]

%%

{digit}+      { printf("Got integer: %s\n", yytext); }
{letter}      { printf("Got variable: %s\n", yytext); }

"+"           { printf("Got plus!\n"); }
"-"           { printf("Got minus!\n"); }
"*"           { printf("Got star!\n"); }
"/"           { printf("Got slash!\n"); }
"("           { printf("Got lpar!\n"); }
")"           { printf("Got rpar!\n"); }
"="           { printf("Got assign!\n"); }
";"           { printf("Got semi!\n"); }

#.*[\n]       { /* comment */ }
{space}*      { /* ignore spacing */ }

.             { fprintf(stderr, "Unexpected char: %s\n", yytext);
                return 1;
              }

%%

int main()
{
  int code = yylex();
  fprintf(stderr, "got code: %d\n", code);
  return 0;
}



%{
#include <stdio.h>

#define INT     300
#define VAR     301
#define PLUS    '+'
//...

%}

%option noyywrap
%option yylineno

digit     [0-9]
space     [ \t\r\n]
letter    [a-z]

%%

{digit}+      { return INT; }
{letter}      { return VAR; }

"+"           { return PLUS; }
"-"           { return MINUS; }
"*"           { return STAR; }
"/"           { return SLASH; }
"("           { return LPAR; }
")"           { return RPAR; }
"="           { return ASSIGN; }
";"           { return SEMI; }

#.*[\n]       { /* comment */ }
{space}*      { /* ignore spacing */ }

.             { fprintf(stderr, "Unexpected char: %s\n", yytext);
                return 1;
              }

%%

int main()
{
  int code;
  yylineno = 1;
  while ( (code=yylex()) )
  {
      printf("On line %3d, got token %3d text '%s'\n",
        yylineno, code, yytext);
  }
  return 0;
}


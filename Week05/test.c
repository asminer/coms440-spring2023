
#include "parser.h"

int main(int argc, const char** argv)
{
    lexer L;
    int i;
    for (i=1; i<argc; i++) {
        init_lexer(&L, argv[i]);
        if (!L.infile) {
            fprintf(stderr, "Couldn't open file %s\n", argv[i]);
            continue;
        }
        int s;
        do {
            s = statement(&L);
            if (1==s) printf("Assignment\n");
            if (2==s) printf("Print\n");
        } while (s);
    }
}

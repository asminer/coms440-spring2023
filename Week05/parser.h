
#ifndef PARSER_H
#define PARSER_H

#include "lexer.h"

typedef struct expr* pexpr;
struct expr {
    token optor;
    pexpr left;         // null for unary minus, leaves
    pexpr right;        // null for leaves
};

typedef struct stmt* pstmt;
struct stmt {
    unsigned var;
    pexpr rhs;      // null if printing
    unsigned lineno;
};

// Returns
//  0 for end of file
//  1 for assignment, i.e,   var = E ;
//  2 for just the variable i.e. var ;
//
pstmt statement(lexer* L);

// E
pexpr expression(lexer* L);

// P
pexpr product(lexer* L);

// T
pexpr term(lexer* L);


#endif

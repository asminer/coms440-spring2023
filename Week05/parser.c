
#include "parser.h"

#include <stdlib.h>

void* mymalloc(unsigned bytes)
{
    void* ptr = malloc(bytes);
    if (!ptr) {
        fprintf(stderr, "Bruh.  Malloc fail.  Bad vibe.\n");
        exit(2);
    }
    return ptr;
}

void error(lexer* L, const char* expect)
{
    fprintf(stderr, "Syntax error file %s line %u:\n\tExpected %s, saw ",
                L->filename, L->lineno, expect);
    switch (L->current.ID) {
        case INT:   fprintf(stderr, "integer\n");
                    break;
        case VAR:   fprintf(stderr, "variable\n");
                    break;
        case END:   fprintf(stderr, "end of file\n");
                    break;
        default:    fprintf(stderr, "%c\n", L->current.ID);
    }

    exit(1);
}

void variable(lexer* L)
{
    if (L->current.ID  != VAR) {
        error(L, "variable");
    }
    next_token(L);
}


pexpr term(lexer* L)
{
    if (L->current.ID == INT || L->current.ID == VAR) {
        pexpr e = mymalloc(sizeof(struct expr));
        e->optor = L->current;
        e->left = 0;
        e->right = 0;
        next_token(L);
        return e;
    }
    if (L->current.ID == '-') {
        pexpr e = mymalloc(sizeof(struct expr));
        e->optor = L->current;
        e->left = 0;
        next_token(L);
        e->right = term(L);
        return e;
    }
    if (L->current.ID == '(') {
        next_token(L);
        pexpr e = expression(L);
        if (L->current.ID != ')') error(L, ")");
        next_token(L);
        return e;
    }
    error(L, "integer or variable");
}

pexpr product(lexer* L)
{
    pexpr left = term(L);
    while (L->current.ID == '*' || L->current.ID == '/') {
        token op = L->current;
        next_token(L);
        pexpr right = term(L);

        pexpr newleft = mymalloc(sizeof(struct expr));
        newleft->optor = op;
        newleft->left = left;
        newleft->right = right;
        left = newleft;
    }
    return left;
}

void expression(lexer* L)
{
    product(L);
    while (L->current.ID == '+' || L->current.ID == '-') {
        next_token(L);
        product(L);
    }
}

int statement(lexer* L)
{
    if (L->current.ID == END) {
        return 0;
    }
    variable(L);
    if (SEMI == L->current.ID) {
        next_token(L);
        return 2;
    }
    if (GETS == L->current.ID) {
        next_token(L);
        expression(L);
        if (SEMI != L->current.ID) error(L, ";");
        next_token(L);
        return 1;
    }
    error(L, "= or ;");
    return 0;
}


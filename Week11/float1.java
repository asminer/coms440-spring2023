
class float1 {

    public static int checkfloat() {
        float f = 1;
        int bits = 0;
        while (f>0) {
          f /= 2.0;
          ++bits;
        }
        return bits;
    }

    public static int  thingy(double v) {
        double[] a = new double[72];
        int i = 0;
        do {
          i = i+1;
        } while (a[i] < v);
        return i;
    }

    public static void main(String args[]) {
        System.out.print("Float divisions to zero: ");
        System.out.println(checkfloat());
    }
}
